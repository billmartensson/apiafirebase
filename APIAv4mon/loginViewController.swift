//
//  loginViewController.swift
//  APIAv4mon
//
//  Created by Bill Martensson on 2017-03-17.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit

import FirebaseAuth

class loginViewController: UIViewController {

    
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextfield: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func doRegister(_ sender: Any) {
        FIRAuth.auth()?.createUser(withEmail: emailTextfield.text!, password: passwordTextfield.text!, completion: { (user, error) in
            print("user registered")
            
            if(error == nil)
            {
                // REG OK!!
                print("Reg ok")
                
                self.dismiss(animated: true, completion: nil)
            } else {
                // REG ERROR
                print("Reg error")
                print(error.debugDescription)
            }
            
        })
    }
    
    @IBAction func doLogin(_ sender: Any) {
        
        FIRAuth.auth()?.signIn(withEmail: emailTextfield.text!, password: passwordTextfield.text!, completion: { (user, error) in
            print("user logged in")
            print(FIRAuth.auth()?.currentUser)
            
            if(error == nil)
            {
                // LOGIN OK!!
                print("Login ok")
                
                self.dismiss(animated: true, completion: nil)
                
            } else {
                // LOGIN ERROR
                print("Login error")
                print(error.debugDescription)
            }
            
        })
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
