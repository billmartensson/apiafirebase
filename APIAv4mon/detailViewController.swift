//
//  detailViewController.swift
//  APIAv4mon
//
//  Created by Bill Martensson on 2017-03-24.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit

class detailViewController: UIViewController {

    var currentShopItem : shoppingItem?
    
    
    @IBOutlet weak var nameTextfield: UITextField!
    
    @IBOutlet weak var boughtSwitch: UISwitch!
    
    
    @IBOutlet weak var descriptionTextview: UITextView!
    
    
    @IBOutlet weak var brandTextfield: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        print("DETAIL viewWillAppear")
        print(currentShopItem?.name)
        
        nameTextfield.text = currentShopItem!.name
        boughtSwitch.isOn = currentShopItem!.bought
        descriptionTextview.text = currentShopItem!.description
        brandTextfield.text = currentShopItem!.brand
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func saveShopping(_ sender: Any) {
        
        currentShopItem!.name = nameTextfield.text!
        currentShopItem!.bought = boughtSwitch.isOn
        currentShopItem!.description = descriptionTextview.text
        currentShopItem!.brand = brandTextfield.text!
        
        performSegue(withIdentifier: "gobacktolist", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        print("PREPARE från detail")
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
