//
//  shoppingTableViewCell.swift
//  APIAv4mon
//
//  Created by Bill Martensson on 2017-03-17.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit

class shoppingTableViewCell: UITableViewCell {
    
    var rowShopItem : shoppingItem?
    
    
    var rownumber = 0
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var boughtSwitch: UISwitch!
    @IBOutlet weak var amountLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    @IBAction func boughtChange(_ sender: Any) {
        print("BOUGHT CHANGE")
        
        //updateRowInfo()
        rowShopItem!.bought = boughtSwitch.isOn
        rowShopItem!.save()
    }
    
    @IBAction func minusAmount(_ sender: Any) {
        print("MINUS")
        
        var amountNumber = Int(amountLabel.text!)!
        amountNumber = amountNumber - 1
        amountLabel.text = String(amountNumber)
        
        rowShopItem!.amount = amountNumber
        rowShopItem!.save()
        //updateRowInfo()
    }
    
    
    @IBAction func plusAmount(_ sender: Any) {
        print("PLUS")
        
        var amountNumber = Int(amountLabel.text!)!
        amountNumber = amountNumber + 1
        amountLabel.text = String(amountNumber)
        
        //updateRowInfo()
        rowShopItem!.amount = amountNumber
        rowShopItem!.save()
    }
    
    func updateRowInfo()
    {
        var rowData = [String : Any?]()
        rowData["rownumber"] = rownumber
        rowData["amount"] = Int(amountLabel.text!)!
        rowData["bought"] = boughtSwitch.isOn
        
        let notificationName = Notification.Name("updaterow")
        NotificationCenter.default.post(name: notificationName, object: rowData)
    }
}
