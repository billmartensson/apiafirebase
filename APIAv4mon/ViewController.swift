//
//  ViewController.swift
//  APIAv4mon
//
//  Created by Bill Martensson on 2017-03-06.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit
import Firebase

class ViewController: UIViewController {

    @IBOutlet weak var firstnameLabel: UILabel!

    var FBref : FIRDatabaseReference?

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        FBref = FIRDatabase.database().reference()
        
        
        
        // Skapa
        var myThing = ["fruit": "Ananas"]
        
        //FBref.child("food").setValue(myThing)
        //FBref.child("food").childByAutoId().setValue(myThing)
        
        
        // Hämta ett värde en gång
        FBref!.child("firstname").observeSingleEvent(of: .value, with: { (snapshot) in
            
            print(snapshot.key)
            print(snapshot.value)
            
            self.firstnameLabel.text = snapshot.value as! String
            
            /*
            // Get user value
            for item in snapshot.children.allObjects as! [FIRDataSnapshot]
            {
                let thisItem = item.value as! NSDictionary
            }
            */
        }) { (error) in
            print(error.localizedDescription)
        }
        
        
        // Hämta ett värde en gång
        FBref!.child("food").observeSingleEvent(of: .value, with: { (snapshot) in
            
            print(snapshot.key)
            print(snapshot.value)
            
            
            // Get user value
            for itemStuffFromFirebase in snapshot.children.allObjects as! [FIRDataSnapshot]
            {
                let thisItem = itemStuffFromFirebase.value as! NSDictionary
                
                print(thisItem.value(forKey: "fruit"))
            }
            
            
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
        // Lyssna efter ändring
        FBref!.child("firstname").observe(.value, with: { (snapshot) in
            self.firstnameLabel.text = snapshot.value as! String
        }) { (error) in
            print(error.localizedDescription)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

