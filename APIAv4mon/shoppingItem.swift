//
//  shoppingItem.swift
//  APIAv4mon
//
//  Created by Bill Martensson on 2017-03-06.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import Foundation
import Firebase

class shoppingItem {
    
    var fbid = ""
    var name = ""
    var bought = false
    var amount = 1
    var description = ""
    var brand = ""
    
    func loadData(key: String, allValues: NSDictionary)
    {
        fbid = key
        name = allValues.value(forKey: "shopName") as! String
        if(allValues.value(forKey: "bought") != nil)
        {
            bought = allValues.value(forKey: "bought") as! Bool
        }
        if(allValues.value(forKey: "amount") != nil)
        {
            amount = allValues.value(forKey: "amount") as! Int
        }
        if(allValues.value(forKey: "description") != nil)
        {
            description = allValues.value(forKey: "description") as! String
        }
        if(allValues.value(forKey: "brand") != nil)
        {
            brand = allValues.value(forKey: "brand") as! String
        }
    }
    
    func save()
    {
        let FBref = FIRDatabase.database().reference()
        
        var shopItem = [String : Any?]()
        
        shopItem["shopName"] = name
        shopItem["amount"] = amount
        shopItem["bought"] = bought
        shopItem["description"] = description
        shopItem["brand"] = brand
        
        var saveToFirebase = FBref.child("usershopping").child((FIRAuth.auth()?.currentUser?.uid)!)
        
        if(fbid == "")
        {
            // Skapa ny
            saveToFirebase = saveToFirebase.childByAutoId()
        } else {
            // Redigera gammal
            saveToFirebase = saveToFirebase.child(fbid)
        }
        
        saveToFirebase.setValue(shopItem, withCompletionBlock: {(error, ref) in
            
            print("SPARA ÄR KLART!")
            if(error != nil)
            {
                print("NÅGOT GICK FEL VID SPARA")
            } else {
                self.fbid = ref.key
            }
        })
    
    }
    
    func deleteMe()
    {
        let FBref = FIRDatabase.database().reference()

        FBref.child("usershopping").child((FIRAuth.auth()?.currentUser?.uid)!).child(fbid).removeValue()
    }
    
}
