//
//  ShoppingViewController.swift
//  APIAv4mon
//
//  Created by Bill Martensson on 2017-03-06.
//  Copyright © 2017 Magic Technology. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

// Bill ändrar saker
// FIXA KRITISK FIX

// Bill jobbar på nästa version
// NY FUNKTION
// YTTERLIGARE NY FUNKTION

// FIXAR INNOVATIV NY FUNKTION

class ShoppingViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    
    @IBOutlet weak var shopNameTextfield: UITextField!
    @IBOutlet weak var shoppingTableview: UITableView!
    
    var stuffToBuy = [shoppingItem]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadTheShoppingList()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if(FIRAuth.auth()?.currentUser == nil)
        {
            performSegue(withIdentifier: "login", sender: nil)
        } else {
            
            print("USER UID")
            print(FIRAuth.auth()?.currentUser?.uid)
            
            
        }
    }

    
    func loadTheShoppingList()
    {
        stuffToBuy.removeAll()
        
        let FBref = FIRDatabase.database().reference()
        
        FBref.child("usershopping").child((FIRAuth.auth()?.currentUser?.uid)!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Hämta shoppinglistan
            for itemStuffFromFirebase in snapshot.children.allObjects as! [FIRDataSnapshot]
            {
                let thisItem = itemStuffFromFirebase.value as! NSDictionary
                
                var loadedShopItem = shoppingItem()
                loadedShopItem.loadData(key: itemStuffFromFirebase.key, allValues: thisItem)
                
                self.stuffToBuy.append(loadedShopItem)
            }
            
            self.shoppingTableview.reloadData()
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func addToShoppinglist(_ sender: Any) {
        
        let addingShopItem = shoppingItem()
        addingShopItem.name = shopNameTextfield.text!
        addingShopItem.amount = 1
        addingShopItem.bought = false
        
        addingShopItem.save()
        
        shopNameTextfield.text = ""
        
        stuffToBuy.append(addingShopItem)
        
        shoppingTableview.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return stuffToBuy.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "shopcell", for: indexPath) as! shoppingTableViewCell
        
        //cell.rownumber = indexPath.row
        cell.rowShopItem = stuffToBuy[indexPath.row]
        
        let currentItem = stuffToBuy[indexPath.row]
        
        cell.nameLabel.text = currentItem.name
        
        cell.amountLabel.text = String(currentItem.amount)
        
        cell.boughtSwitch.isOn = currentItem.bought
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        performSegue(withIdentifier: "detail", sender: indexPath.row)
    }

    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        
        return true
    }
    
    func tableView(_ tableView: UITableView, willBeginEditingRowAt indexPath: IndexPath) {
        
        print("willBeginEditingRowAt")
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        
        print("commit editingStyle")
        
        if(editingStyle == .delete)
        {
            stuffToBuy[indexPath.row].deleteMe()
            stuffToBuy.remove(at: indexPath.row)
            
            shoppingTableview.reloadData()
        }
    }
    
    
    @IBAction func doLogout(_ sender: Any) {
        
        do {
            try FIRAuth.auth()?.signOut()
            
            stuffToBuy.removeAll()
            shoppingTableview.reloadData()
            
            performSegue(withIdentifier: "login", sender: nil)
        } catch {
            print("KAN INTE LOGGA UT!!")
        }
    }
    
    
    @IBAction func editTheList(_ sender: Any) {
        
        if(shoppingTableview.isEditing == true)
        {
            shoppingTableview.isEditing = false
        } else {
            shoppingTableview.isEditing = true
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if(segue.identifier == "detail")
        {
            let dest = segue.destination as! detailViewController
            
            dest.currentShopItem = stuffToBuy[sender as! Int]
        }
    }
    
    
    @IBAction func backfromDetail(segue: UIStoryboardSegue)
    {
        print("VI ÄR TILLBAKA FRÅN DETAIL!!")
        
        let src = segue.source as! detailViewController
        
        src.currentShopItem!.save()
        
        shoppingTableview.reloadData()
    }
    
    @IBAction func deletefromDetail(segue: UIStoryboardSegue)
    {
        print("DAGS ATT RADERA")
        
        let src = segue.source as! detailViewController
        
        for index in 0...stuffToBuy.count-1
        {
            if(stuffToBuy[index].fbid == src.currentShopItem!.fbid)
            {
                stuffToBuy[index].deleteMe()
                stuffToBuy.remove(at: index)
                break
            }
        }
        
        shoppingTableview.reloadData()
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
